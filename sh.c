#define MAXARGS   128
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <setjmp.h>
#include <signal.h>
#include <sys/wait.h>
#include <errno.h>

#define	MAXLINE	 8192  /* Max text line length */


/* Function prototypes */
void eval(char *cmdline);
int parseline(char *buf, char **argv);
int builtin_command(char **argv); 
void sigint_handler(int sig);
void sigterm_handler(int sig);
void show_help();

int main(int argc, char **argv) 
{
	char cmdline[MAXLINE]; /* Command line */
	signal(SIGINT,sigint_handler);
	signal(SIGTERM, sigterm_handler);
	char prompt[MAXLINE];
	*prompt = 0;
	strcat(prompt,"$ ");
	while (1) {
		/* Read */
		printf("%s",prompt);                   
		fflush(0);
		fgets(cmdline, MAXLINE, stdin); 
		if (feof(stdin))
			exit(0);

		/* Evaluate */
		eval(cmdline);
	} 
}

void unix_error(char *msg) /* Unix-style error */
{
	fprintf(stderr, "%s: %s\n", msg, strerror(errno));
	exit(0);
}


/* $end shellmain */

/* $begin eval */
/* eval - Evaluate a command line */
void eval(char *cmdline) 
{
	char *argv[MAXARGS]; /* Argument list execve() */
	char buf[MAXLINE];   /* Holds modified command line */
	pid_t pid;           /* Process id */

	strcpy(buf, cmdline);
	parseline(buf, argv); 
	if (argv[0] == NULL)  
		return;   /* Ignore empty lines */

	if (!builtin_command(argv)) { 
		if ((pid = fork()) == 0) {   /* Child runs user job */
			if (execvp(argv[0], argv) < 0) {
				printf("%s: Command not found.\n", argv[0]);
				exit(1);
			}
		}

		/* Parent waits for foreground job to terminate */
		int status;
		if (waitpid(pid, &status, 0) < 0)
			unix_error("waitfg: waitpid error");
		else{
			if(WIFEXITED(status)){
				int exit_code = WEXITSTATUS(status);
			}
		}
	}
	return;
}


/*Show help text*/
void show_help(){
	puts("Shell loosely based on CMU's shellex.c starter code.");
	puts("Built in commands:");
	puts("	quit: exits shell");
	puts("	exit: exits shell");
	puts("	help: prints this help text");
	puts("	pid: prints shell's process identifier");
	puts("	ppid: prints shell's parent's process identifier");
	puts("	pwd: prints the working directory");
	puts("	cd <path>: changes the working directory to the value of <path>");
	puts("The prompt can be specified with -p <prompt> when running the shell.");
}


/* Sigterm handler */
void sigterm_handler(int sig){
	exit(0);
}

/* Sigint handler*/
void sigint_handler(int sig){
	return;
}

/* If first arg is a builtin command, run it and return true */
int builtin_command(char **argv) 
{
	if (!strcmp(argv[0], "quit")) /* quit command */
		raise(SIGTERM);
	if (!strcmp(argv[0], "exit")) /* exit command */
		raise(SIGTERM);
	if (!strcmp(argv[0], "pid")){
		printf("%d\n",getpid());
		return 1;
	}
	if (!strcmp(argv[0], "ppid")){
		printf("%d\n",getppid());
		return 1;
	}
	if (!strcmp(argv[0], "help")){
		show_help();
		return 1;
	}
	if (!strcmp(argv[0], "pwd")){
		char cwd[MAXLINE];
		getcwd(cwd, sizeof(cwd));
		printf("%s\n",cwd);
		return 1;
	}
	if (!strcmp(argv[0], "cd")){
		char cwd[MAXLINE];
		getcwd(cwd, sizeof(cwd));
		if(argv[1] == NULL){
			//printf("%s\n",cwd);
		} else {
			if(*argv[1] == '/'){
				sprintf(cwd, "%s", argv[1]);
			} else {
				strcat(cwd,"/");
				strcat(cwd,argv[1]);
			}
			if(chdir(cwd) != 0){
				printf("Cannot cd to %s\n",cwd);
			}
		}
		return 1;
	}
	if (!strcmp(argv[0], "&"))    /* Ignore singleton & */
		return 1;
	return 0;                     /* Not a builtin command */
}
/* $end eval */

/* $begin parseline */
/* parseline - Parse the command line and build the argv array */
int parseline(char *buf, char **argv) 
{
	char *delim;         /* Points to first space delimiter */
	int argc;            /* Number of args */
	int bg;              /* Background job? */

	buf[strlen(buf)-1] = ' ';  /* Replace trailing '\n' with space */
	while (*buf && (*buf == ' ')) /* Ignore leading spaces */
		buf++;

	/* Build the argv list */
	argc = 0;
	while ((delim = strchr(buf, ' '))) {
		argv[argc++] = buf;
		*delim = '\0';
		buf = delim + 1;
		while (*buf && (*buf == ' ')) /* Ignore spaces */
			buf++;
	}
	argv[argc] = NULL;

	if (argc == 0)  /* Ignore blank line */
		return 1;

	//return bg;
	/*Never background a process as that is not a requirement*/
	return 0;
}
/* $end parseline */

