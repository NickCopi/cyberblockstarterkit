#include <dirent.h> 
#include <stdio.h> 

int main(int argc, const char *argv[]){
	DIR *dp;
	struct dirent *ep;

	dp = opendir ("./");
	if (dp != NULL)
	{
		while (ep = readdir (dp))
			puts (ep->d_name);
		(void) closedir (dp);
	}
	else
		perror ("Couldn't open the directory");

	return 0;  
}
