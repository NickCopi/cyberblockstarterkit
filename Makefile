
all: sh ls vi

sh: sh.c
	gcc sh.c -o sh
ls: ls.c
	gcc ls.c -o ls
vi: vi.c
	gcc vi.c -o vi
